import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BfButtonDirective } from './directives/bf-button.directive';
import { BfButtonDarkDirective } from './directives/bf-button-dark.directive';
import { BfButtonLightDirective } from './directives/bf-button-light.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BfButtonDirective,
    BfButtonDarkDirective,
    BfButtonLightDirective
  ],
  exports: [
    BfButtonDirective,
    BfButtonDarkDirective,
    BfButtonLightDirective
  ]
})
export class ButtonsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ButtonsModule,
      providers: []
    };
  }
}
