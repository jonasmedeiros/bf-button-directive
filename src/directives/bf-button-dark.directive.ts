import { Directive, OnInit, Input, Renderer, ElementRef } from '@angular/core';

@Directive({
  selector: '[bf-button-dark]'
})
export class BfButtonDarkDirective implements OnInit{

  @Input() size: string = 'medium';
  @Input() full: boolean = false;

  constructor(public el: ElementRef, public renderer: Renderer) {}

  ngOnInit() {
    this.renderer.setElementClass(this.el.nativeElement, 'bf', true);
    this.renderer.setElementClass(this.el.nativeElement, 'transparent-dark', true);
    this.renderer.setElementClass(this.el.nativeElement, this.size, true);

    if (this.full) {
      this.renderer.setElementClass(this.el.nativeElement, 'full-width', true);
    }
  }
}
